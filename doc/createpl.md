# Create your own programming language
## (by following rules)

Eric Conlon @econlon

---

# Goals

Make a simple but powerful language for fun (the Simply Typed Lambda Calculus)

Learn how to read through formalism in PL texts

# Non-goals

Cover parsing or compilation

# Prerequisites

You'll need to know how to write programs in Scala

---

# Terms of a simple langauge

Natural numbers (Z >= 0) with addition

    4

    1 + 3

    1 + (2 + 1)

    (1 + 2) + 1

    (1 + 1) + (1 + 1)

---

# Terms as trees

Our Abstract Syntax Tree (AST) definition for this language:

    sealed trait Term
    case class Num(value: Int) extends Term
    case class Plus(left: Term, right: Term) extends Term

    // (1 + 2) + 1
    Plus(Plus(Num(1), Num(2)), Num(1))

---

# Independent of host language

We'll use base one to represent numbers.  Zero, Succ(Zero), Succ(Succ(Zero))...

    sealed trait Term
    case object Zero extends Term
    case class Succ(term: Term) extends Term  // Successor of, i.e. term + 1
    case class Plus(left: Term, right: Term) extends Term

    // (1 + 2) + 1
    Plus(Plus(Succ(Zero), Succ(Succ(Zero))), Succ(Zero))

Shorthand: In your head, identify Succ(Zero) with 1 and so on!

---

# Reducing terms to values

This term

    // 1 + 2
    Plus(Succ(Zero), Succ(Succ(Zero)))

can be reduced to the equivalent term by "moving the Succ right"

    // 0 + 3
    Plus(Zero, Succ(Succ(Succ(Zero))))

which can be reduced to the equivalent term by "dropping the Zero"

    // 3
    Succ(Succ(Succ(Zero)))

which cannot be reduced further.

---

# Terms, values, reduction

Reduction rules tell us how to evaluate terms

    // 0 + 1 -> 1
    Plus(Zero, Succ(Zero)) -> Succ(Zero)

Values are terms that cannot be reduced

    Zero  // is a value

    Plus(Zero, Zero)  // is NOT a value

---

# Formal declaration of terms and values

"Metavariables" t and v (along with t1, t1', v1, etc) specify collections of terms and values

    t := Zero | Succ t | Plus t t

    v := Zero | Succ v

Note: Will to be dropping delimiters in this context. Plus(t, t) == Plus t t

Remember: values are subset of terms

---

# More metavariables

Complicated languages may use more metavariables:

    t := True | False | Zero | Succ t | ...

    bool := True | False

    num := Zero | Succ num

    v := num | bool

---

# Rules and judgements

"If X then Y" (X is hypothesis, Y is conclusion)

    X
    -
    Y

"If X and W then Y"

    X   W
    -----
    Y

"X evaluates to Y" (given; conclusion always holds)

    ------
    X -> Y

Rules can have names and involve judgements (predicates):

"Rule Foo: If P(X) then P(Y)"

    P(X)
    ---- (Foo)
    P(Y)

---

# Proof trees

Given "y = 1 + 1" and "x = 1 + y", what is the value of x?

                       --------- (given)
                       y = 1 + 1
    --------- (given)  --------- (plus)
    x = 1 + y          y = 2
    ---------------------------- (substitution)
    x = 1 + 2
    ---------------------------- (plus)
    x = 3

Working backward: How do we know "x = 3"? We know it if "x = 1 + 2"...

---

# Metavariables as rules

    num := Zero | Succ(num)

  can also be expressed as

    -------------
    Zero is a num

    v is a num
    ----------------
    Succ(v) is a num

---

# Evaluation rules

    t -> t'

means t evaluates to t' in one step.

Given these rules:

    ------------------ (E-PlusZero)
    Plus(Zero, t) -> t

    ---------------------------------------- (E-PlusOne)
    Plus(Succ(t1), t2) -> Plus(t1, Succ(t2))

The expression

    Plus(Succ(Zero), Zero)

evaluates this in one step:

    Plus(Zero, Succ(Zero))

and evaluates to this in the end:

    Succ(Zero)

One step: "small-step operational semantics"
All steps: "big-step ..."

---

# Rules for this language

    t := Zero | Succ t | Plus t t

    v := Zero | Succ v

    t -> t'
    ------------------- (E-Succ)
    Succ(t) -> Succ(t')

    t1 -> t1'
    ----------------------------- (E-PlusLeft)
    Plus(t1, t2) -> Plus(t1', t2)

    t2 -> t2'
    ----------------------------- (E-PlusRight)
    Plus(t1, t2) -> Plus(t1, t2')

    ------------------ (E-PlusZero)
    Plus(Zero, t) -> t

    ---------------------------------------- (E-PlusOne)
    Plus(Succ(t1), t2) -> Plus(t1, Succ(t2))

---

# Where do we reduce?

Which "redex" ("REDucible EXpression") do we choose?

Left or right redex first?

    Plus(Plus(Zero, Zero), Plus(Zero, Zero)) ->

    Plus(Zero, Plus(Zero, Zero))  // Left

    Plus(Plus(Zero, Zero), Zero)  // Right

Outer or inner redex first?

    Plus(Zero, Plus(Succ(Zero), Zero)) ->

    Plus(Succ(Zero), Zero)  // Outer

    Plus(Zero, Plus(Zero, Succ(Zero)))  // Inner

We get to choose our strategy!

---

# Call by value strategy

The one you're probably most familiar with:

Evaluate outermost redex (not inside a function)

AFTER right hand side has been evaluated to a value.

Corresponds to evaluating function arguments left to right before calling.

    Plus(Plus(Zero, Zero), Plus(Zero, Zero)) ->

    Plus(Zero, Plus(Zero, Zero))  // Left

    Plus(Zero, Plus(Succ(Zero), Zero)) ->

    Plus(Zero, Plus(Zero, Succ(Zero)))  // Inner

We will use this.

---

# Call by name strategy

A "lazier" approach: choose leftmost outermost redex (not inside a function).

    Plus(Zero, Plus(Succ(Zero), Zero)) ->

    Plus(Succ(Zero), Zero)  // Outer

Does not evaluate unused function arguments.

Be careful: can evaluate substituted expressions twice!

---

# Exercise time!

Fill in "ArithSpec.scala" with your own small-step evaluator.

---

# Adding booleans and ifs

    t := Zero | Succ t | Pred t | IsZero t | True | False | IfThenElse t t t

    v := Zero | Succ v | True | False

    sealed trait Term
    case object Zero extends Term
    case class Succ(arg: Term) extends Term
    case class Pred(arg: Term) extends Term
    case class IsZero(arg: Term) extends Term
    case object True extends Term
    case object False extends Term
    case class IfThenElse(guard: Term, thenCase: Term, elseCase: Term) extends Term

---

# New evaluation rules

    t -> t'
    ------------------- (E-Pred)
    Pred(t) -> Pred(t')

    ------------------ (E-PredZero)
    Pred(Zero) -> Zero

    ------------------ (E-PredSucc)
    Pred(Succ(v)) -> v

    t -> t'
    ----------------------- (E-IsZero)
    IsZero(t) -> IsZero(t')

    -------------------- (E-IsZeroZero)
    IsZero(Zero) -> True

    ------------------------ (E-IsZeroSucc)
    IsZero(Succ(v)) -> False

(Cont.)

---

# If rules

    t1 -> t1'
    ------------------------------------------------- (E-IfGuard)
    IfThenElse(t1, t2, t3) -> IfThenElse(t1', t2, t3)

    t2 -> t2'
    ------------------------------------------------- (E-IfThen)
    IfThenElse(t1, t2, t3) -> IfThenElse(t1, t2', t3)

    t3 -> t3'
    ------------------------------------------------- (E-IfElse)
    IfThenElse(t1, t2, t3) -> IfThenElse(t1, t2, t3')

    ------------------------------------------------- (E-IfTrue)
    IfThenElse(True, t2, t3) -> t2

    ------------------------------------------------- (E-IfFalse)
    IfThenElse(False, t2, t3) -> t3

---

# Exercise time!

Fill in "IfSpec.scala" with your own small-step evaluator.

---

# Introducing functions and variables

Given a term t1, we can define functions ("abstractions"):

    (λx. t1)

    Abs("x", t1)

Occurrences of Var("x") in t1 we call "bound variables."

Other variables Var("y") that aren't under some binder "λy" we call "free variables."

    Var("x")  // x is free

    Abs("x", Var("x"))  // x is bound

    Abs("x", Var("y"))  // y is free

Be careful:

    Abs("x", Abs("x", Var("x")))  // x is bound... to the innermost binder!

---

# Partial application

We take for granted functions of multiple arguments can be converted to functions of one:

    (λx y z. t1) <==> (λx. (λy. (λz. t1)))

Scala does this for us:

    def f(x: Int)(y: Int)(z: Int): Int = ...

    var fx: (Int => Int => Int) = f(1)

    var fxy: (Int => Int) = fx(2)

---

# Applications and substitutions

We evaluate applications through substitutions:

    (λx. t1)(t2) -> [x -> t2](t1)

This substitution replaces all free variables name

"Lambda x dot t1 applied to t2 evaluates to t2 substituted for x in t1!"

    App(Abs("x", Var("x")), True) -> True

    App(Abs("x", Var("y")), True) -> Var("y")

    App(Abs("x", Abs("x", Var("x"))), True) -> Abs("x", Var("x"))

---

# Contexts

---

# Adding functions

    t := ... | x | t t | λx. t

    v := ... | x | λx. t

    sealed trait Term
    // ... all previous constructors ...
    case class Var(name: String) extends Term
    case class App(first: Term, second: Term) extends Term
    case class Abs(name: String, body: Term) extends Term

---

# Function rules

---

# Exercise time!

Fill in "FuncSpec.scala" with your own small-step evaluator.

---

# Types

We simply can't evaluate some terms like

    If(Zero, Zero, False)

We can associate a judgement "has type X" with some terms and call those terms "well-typed."

Well-typed terms
* make progress: we can evaluate them to another term in one step.
* preserve types: whatever we evaluate the term to, that term will have the same type.

---

# Typing rules

---

# Further resources

"Types and Programming Langauges" by Benjamin Pierce